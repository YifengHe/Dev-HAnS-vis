// Copyright 2021 Kenny Bang, Johan Berg, Seif Bourogaa, Lucas Frövik, Alexander Grönberg, Sara Persson

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// https://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package se.ch.hans.annotations;

import se.ch.hans.annotations.file.iFileAnnotationUtil;
import se.ch.hans.annotations.folder.iFolderAnnotationUtil;
import se.ch.hans.annotations.infile.iInFileAnnotationUtil;

import java.io.IOException;
import java.util.*;

public final class FeatureAnnotationUtil {
    private FeatureAnnotationUtil() {}

    public static Map<String, ArrayList<Object>> getFeatureAnnotationLocations(String path) throws IOException {
        return getFeatureMappings(path);
    }

    public static Map<String, ArrayList<Object>> getFeatureAnnotationTanglings(String path) throws IOException{
        return getFeatureTangling(path);
    }

    private static Map<String, ArrayList<Object>> getFeatureTangling(String path) throws IOException{
        Map<String, ArrayList<Object>> featureAnnotationTanglings = new HashMap<>();
        Map<String, ArrayList<ArrayList<String>>> codeTanglings = getCodeTanglings(path);
        Map<String, ArrayList<ArrayList<String>>> fileTanglings = getFileTanglings(path);
        Map<String, ArrayList<ArrayList<String>>> folderTanglings = getFolderTanglings(path);

        for(String feature : getAllFeatures(codeTanglings, fileTanglings, folderTanglings)){
            featureAnnotationTanglings.put(feature, new ArrayList<>());
        }

        for(String key : featureAnnotationTanglings.keySet()){
            // Add infile annotations.
            if(codeTanglings.containsKey(key)){
                featureAnnotationTanglings.get(key).add(0, codeTanglings.get(key));
            } else {
                featureAnnotationTanglings.get(key).add(0, new ArrayList<>());
            }

            // Add file annotations.
            if(fileTanglings.containsKey(key)){
                featureAnnotationTanglings.get(key).add(1, fileTanglings.get(key));
            } else {
                featureAnnotationTanglings.get(key).add(1, new ArrayList<>());
            }

            // Add folder annotations.
            if(folderTanglings.containsKey(key)){
                featureAnnotationTanglings.get(key).add(2, folderTanglings.get(key));
            } else {
                featureAnnotationTanglings.get(key).add(2, new ArrayList<>());
            }
        }

        return featureAnnotationTanglings;
    }

    private static Map<String, ArrayList<Object>> getFeatureMappings(String path) throws IOException {
        Map<String, ArrayList<Object>> featureAnnotationLocations = new HashMap<>();
        Map<String, ArrayList<ArrayList<String>>> infileAnnotations = getCodeAnnotations(path);
        Map<String, ArrayList<ArrayList<String>>> fileAnnotations = getFileAnnotations(path);
        Map<String, ArrayList<ArrayList<String>>> folderAnnotations = getFolderAnnotations(path);

        for(String feature : getAllFeatures(infileAnnotations, fileAnnotations, folderAnnotations)){
            featureAnnotationLocations.put(feature, new ArrayList<>());
        }

        for(String key : featureAnnotationLocations.keySet()){
            // Add infile annotations.
            if(infileAnnotations.containsKey(key)){
                featureAnnotationLocations.get(key).add(0, infileAnnotations.get(key));
            } else {
                featureAnnotationLocations.get(key).add(0, new ArrayList<>());
            }

            // Add file annotations.
            if(fileAnnotations.containsKey(key)){
                featureAnnotationLocations.get(key).add(1, fileAnnotations.get(key));
            } else {
                featureAnnotationLocations.get(key).add(1, new ArrayList<>());
            }

            // Add folder annotations.
            if(folderAnnotations.containsKey(key)){
                featureAnnotationLocations.get(key).add(2, folderAnnotations.get(key));
            } else {
                featureAnnotationLocations.get(key).add(2, new ArrayList<>());
            }
        }

        return featureAnnotationLocations;
    }

    private static Set<String> getAllFeatures(Map<String, ArrayList<ArrayList<String>>> codeAnnotations, Map<String, ArrayList<ArrayList<String>>> fileAnnotations, Map<String, ArrayList<ArrayList<String>>> folderAnnotations) {
        Set<String> features = new HashSet<>();
        features.addAll(codeAnnotations.keySet());
        features.addAll(fileAnnotations.keySet());
        features.addAll(folderAnnotations.keySet());
        return features;
    }

    private static  Map<String, ArrayList<ArrayList<String>>> getCodeAnnotations(String path) throws IOException {
        return iInFileAnnotationUtil.getCodeAnnotations(path);
    }

    private static  Map<String, ArrayList<ArrayList<String>>> getCodeTanglings(String path) throws IOException {
        return iInFileAnnotationUtil.getCodeTanglings(path);
    }

    private static Map<String, ArrayList<ArrayList<String>>> getFileAnnotations(String path) throws IOException {
        return iFileAnnotationUtil.getFileAnnotations(path);
    }

    private static Map<String, ArrayList<ArrayList<String>>> getFileTanglings(String path) throws IOException {
        return iFileAnnotationUtil.getFileTanglings(path);
    }

    private static Map<String, ArrayList<ArrayList<String>>> getFolderAnnotations(String path) throws IOException {
        return iFolderAnnotationUtil.getFolderAnnotations(path);
    }

    private static Map<String, ArrayList<ArrayList<String>>> getFolderTanglings(String path) throws IOException {
        return iFolderAnnotationUtil.getFolderTanglings(path);
    }

}
